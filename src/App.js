import React, { useEffect, Suspense } from 'react';
import { Switch, Route, useRouteMatch } from 'react-router-dom';
import './App.scss';

import ScrollToTop from './components/ReusableComponents/ScrollToTop/ScrollToTop';
import CarDetail from './components/FilterWrapper/Cars/CarDetail/CarDetail';
import MainPage from './components/MainPage/MainPage';


function App() {
  const match = useRouteMatch("/:id");

  useEffect(() => {
    if (match) {
      document.body.classList.add("overflow-hidden");
    }
    else {
      document.body.classList.remove("overflow-hidden");
    }
  }, [match]); 


  return (
    <Suspense fallback="loading">
      <div className="App">
          <MainPage></MainPage>
          <Switch>
              <Route path="/:id" exact component={CarDetail}>
              </Route>
          </Switch>
        <ScrollToTop></ScrollToTop>
      </div>
    </Suspense>
  );
}

export default App;
