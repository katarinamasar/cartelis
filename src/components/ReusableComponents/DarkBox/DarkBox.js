import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';

import './DarkBox.scss';


class BaseDarkBox extends Component {
    render() {
        return (
            <div className="dark-box">
                <div className="dark-box--days">{this.props.numOfDays} {this.props.days}</div>
                {this.props.price.length <= 4 ? 
                    <div className="dark-box--price">{this.props.price} €<span className="dark-box--price-small">/{this.props.t("carDetail.day")}</span></div>
                    : 
                    <div className="dark-box--price">{this.props.price}</div>
                }
                {this.props.price.length <= 4 ?
                    <div className="orange-btn-wrapper">
                        <a href={"mailto:rent@cartelis.sk?subject=" + this.props.subject + " €/" + this.props.t("carDetail.day")} className="orange-btn">{this.props.t("carDetail.placeOrder")}</a>
                    </div>
                    :
                    <div className="orange-btn-wrapper">
                        <a href={"mailto:rent@cartelis.sk?subject=" + this.props.subject + this.props.t("carDetail.byAgreement")} className="orange-btn">{this.props.t("carDetail.makeAgreement")}</a>
                    </div>
                }
            </div>
        );
    }
}

export default withTranslation()(BaseDarkBox);