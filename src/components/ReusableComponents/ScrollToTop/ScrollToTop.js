import React, { Component } from 'react';
import { animateScroll as scroll } from "react-scroll";

import './ScrollToTop.scss';

class ScrollToTop extends Component {
    state = {
        scrollPos: 0
    }

    componentDidMount() {
        window.addEventListener('scroll', this.handleScroll);
    }
    
    handleScroll = () => {
        let scrollPos = document.documentElement.scrollTop;
        this.setState({scrollPos: scrollPos});
    }

    render() {

        return (
           <div className={["scroll-to-top--wrapper", this.state.scrollPos > 1500 ? "" : "hide"].join(" ")} onClick={() => scroll.scrollToTop()}>

           </div>
        );
    }
}

export default ScrollToTop;