import React, { Component } from 'react';

import headingGlow from '../../../assets/images/heading_glow.png';

import './HeadingMain.scss';


class H1 extends Component {
    render() {
        return (
            <div className={this.props.type + " headingMain"}>
                <div className="headingMain--line"></div>
                <div className="headingMain--glowWrapper">
                    <img src={headingGlow} className="headingMain--glow" alt="pozadie nadpisu"/>
                </div>
                <div className="center-wrapper">
                    <div className="headingMain--h1-wrapper">
                        <h1>{this.props.sectionName}</h1>
                    </div>
                </div>
            </div>
        );
    }
}

export default H1;