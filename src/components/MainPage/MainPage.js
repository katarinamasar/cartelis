import React from 'react';

import Header from '../Header/Header';
import FilterWrapper from '../FilterWrapper/FilterWrapper';
import Services from '../Services/Services';
import Conditions from '../Conditions/Conditions';
import Contact from '../Contact/Contact';
import AboutUs from '../AboutUs/AboutUs';
import Footer from '../Footer/Footer';

function MainPage() {
    return (
        <div className="home">
            <Header></Header>
            <div className="main">
                <FilterWrapper></FilterWrapper>
                <Services></Services>
                <Conditions></Conditions>
                <AboutUs></AboutUs>
                <Contact></Contact>
            </div>
            <Footer></Footer>
        </div>
    )
}

export default MainPage;