import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';

import './Footer.scss';


class BaseFooter extends Component {
    render() {
        return (
            <div>
                <div className="container">
                <div className="footer--logoFull">
                        <img src={process.env.PUBLIC_URL + '/images/logo_footer.png'} alt="logo" className="footer--logo"/>
                    </div>
                </div>
                <div className="center-wrapper">
                    <a href="http://www.gabrielli-design.com" target="_blank" rel="noopener noreferrer" className="footer--copyright">©2020 Cartelis s.r.o.  {this.props.t("footerRights")}  Design by Gabrielli</a>
                </div>
            </div>
        );
    }
}

export default withTranslation()(BaseFooter);