import React, { Component } from 'react';

import Cars from './Cars/Cars';
import Filter from './Filter/Filter';

const cars = require('./Cars/carList.json')

function filterCars(brand = 'all', type = 'all', sort = 'priceDesc') {
    let filteredCars = cars;
        
    if (brand !== 'all') {
        filteredCars = filteredCars.filter(car => car.brand === brand);
    }

    if (type !== 'all') {
        filteredCars = filteredCars.filter(car => (
            car.detail.bodywork.toLowerCase().split(", ").includes(type)
        ));
    }

    if (sort === 'priceAsc') {
        filteredCars = filteredCars.sort((a,b) => a.price - b.price);
    }

    if (sort === 'priceDesc') {
        filteredCars = filteredCars.sort((a,b) => b.price - a.price);
    }

    if (sort === 'power') {
        filteredCars = filteredCars.sort((a,b) => b.detail.power - a.detail.power);
    }

    return filteredCars;
}


class FilterWrapper extends Component {
    state = {
        filterResults: filterCars()
    }

    showResults = (brand, type, sort) => {
        const filteredCars = filterCars(brand, type, sort);

        this.setState({filterResults: filteredCars});
    }
        
    render() {
        return (
            <section>
                <Filter filterConditions={this.showResults}></Filter>
                <Cars filteredCars={this.state.filterResults}></Cars>
            </section>
        );
    }
}

export default FilterWrapper;