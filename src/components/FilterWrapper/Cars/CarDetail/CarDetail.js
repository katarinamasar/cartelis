import React, { Component } from 'react';

import Footer from '../../../Footer/Footer';
import Header2 from '../../../Header/Header2';
import DarkBox from '../../../ReusableComponents/DarkBox/DarkBox';
import { withTranslation } from 'react-i18next';
import i18n from '../../../../i18n';


import './CarDetail.scss';
import '../CarCard/CarCard.scss';


const data = require('../carList.json');
class BaseCarDetail extends Component {
    selectedCar;
    urlParams;
    carId;

    goBack = () => {
        this.props.history.push({ pathname: '/', search: `lng=${i18n.language}` });
    }
    
    render() {
        this.urlParams = this.props.match.params.id.split('_');
        this.carId = +this.urlParams.slice(-1);
        this.selectedCar = data.find((car) => car.id === this.carId );

        return (
                <div className="carDetail--bg">
                    <div className="carDetail">
                        <Header2></Header2>
                        <div className="container car-detail">
                            <div className="car-detail--wrapper">
                                <div className="flex flex-between car-detail--top">
                                    <div className="flex car-detail--brand-wrapper">
                                        <img src={process.env.PUBLIC_URL + this.selectedCar.logo} alt="logo" className="car-detail--logo" />
                                        <div className="flex wrap">
                                            <div className="car-detail--h1">{this.selectedCar.brand}</div>
                                            <div className="car-detail--h2">{this.selectedCar.typeFull ? this.selectedCar.typeFull : this.selectedCar.type}</div>
                                        </div>
                                    </div>
                                    <div className="orange-btn-wrapper" onClick={this.goBack}>
                                        <div className="orange-btn">{this.props.t("carDetail.close")}</div>
                                    </div>
                                </div>

                                <div className="flex flex-between car-detail--middle-part">
                                    <div className="slick">
                                        <div className="carCard--imageWrapper">
                                            <img src={process.env.PUBLIC_URL + this.selectedCar.detail.images[0]} alt="" className="carCard--image"/>
                                        </div>
                                    </div>
                                    <div className="car-detail--detail">
                                        <div className="car-detail--text">
                                            <p className="car-detail--p">
                                                {this.props.t("carDetail.seats")}: <span className="car-detail--span">{this.selectedCar.detail.seats}</span>
                                            </p>
                                            <p className="car-detail--p">
                                                {this.props.t("carDetail.transmission")}: <span className="car-detail--span">{this.selectedCar.detail.transmissionNum}{this.props.t(this.selectedCar.detail.transmission)}</span>
                                            </p>
                                            <p className="car-detail--p">
                                                {this.props.t("carDetail.fuel")}: <span className="car-detail--span">{this.props.t(this.selectedCar.detail.fuel)}</span>
                                            </p>
                                            <p className="car-detail--p">
                                                {this.props.t("carDetail.body")}: <span className="car-detail--span">{this.props.t(this.selectedCar.detail.bodywork)}</span>
                                            </p>
                                            <p className="car-detail--p">
                                                {this.props.t("carDetail.cylinders")}: <span className="car-detail--span">{this.props.t(this.selectedCar.detail.rollers)}</span>
                                            </p>
                                            <p className="car-detail--p">
                                                {this.props.t("carDetail.drive")}: <span className="car-detail--span">{this.props.t(this.selectedCar.detail.drive)}</span>
                                            </p>
                                            <p className="car-detail--p">
                                                {this.props.t("carDetail.maxSpeed")}: <span className="car-detail--span">{this.selectedCar.detail.maxSpeed}</span>
                                            </p>
                                            <p className="car-detail--p">
                                                {this.props.t("carDetail.acceleration")} (0 - 100km/h): <span className="car-detail--span">{this.selectedCar.detail.acceleration}</span>
                                            </p>
                                            <p className="car-detail--p">
                                                {this.props.t("carDetail.engine")}: <span className="car-detail--span">{this.selectedCar.detail.volume}</span>
                                            </p>
                                            <p className="car-detail--p">
                                                {this.props.t("carDetail.torque")}: <span className="car-detail--span">{this.selectedCar.detail.moment}</span>
                                            </p>
                                            <p className="car-detail--p">
                                                {this.props.t("carDetail.equipment")}: <span className="car-detail--span">{this.props.t(this.selectedCar.detail.equipment)}</span>
                                            </p>
                                        </div>
                                        <div className="flex flex-center wrap car-detail--icons">
                                            <div className="car-detail--icon-wrapper">
                                                <div className="flex-col">
                                                    <img src={process.env.PUBLIC_URL + 'images/icon_speed.png'} alt="rýchlosť" className="car-detail--icon" />
                                                    <p className="car-detail--description">{this.selectedCar.detail.maxSpeed}</p>
                                                </div>
                                            </div>
                                            <div className="car-detail--icon-wrapper">
                                                <div className="flex-col">
                                                    <img src={process.env.PUBLIC_URL + 'images/icon_acceleration.png'} alt="rýchlosť" className="car-detail--icon" />
                                                    <p className="car-detail--description">{this.selectedCar.detail.acceleration}</p>
                                                </div>
                                            </div>
                                            <div className="car-detail--icon-wrapper">
                                                <div className="flex-col">
                                                    <img src={process.env.PUBLIC_URL + 'images/icon_gearbox.png'} alt="rýchlosť" className="car-detail--icon" />
                                                    <p className="car-detail--description">{this.selectedCar.detail.transmissionNum}{this.props.t(this.selectedCar.detail.transmission)}</p>
                                                </div>
                                            </div>
                                            <div className="car-detail--icon-wrapper">
                                                <div className="flex-col">
                                                    <img src={process.env.PUBLIC_URL + 'images/icon_roller.png'} alt="rýchlosť" className="car-detail--icon" />
                                                    <p className="car-detail--description">{this.props.t(this.selectedCar.detail.rollers)}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="flex flex-end car-detail--price">
                                            {typeof(this.selectedCar.price) === 'number' ?
                                                <div className="flex flex-col flex-align-center car-detail-price-wrapper">
                                                    <div className="flex car-detail--priceFrom">
                                                        <div className="flex">{this.props.t("carDetail.fromOnly")}</div> 
                                                        <h1 className="car-detail--superh1">{this.selectedCar.price} €</h1> 
                                                        <div>/{this.props.t("carDetail.day")}</div>
                                                    </div>    
                                                    <div className="carDetail--small">{this.props.t("carDetail.taxExcluded")}</div>
                                                </div>
                                                :
                                                <></>
                                            }
                                            <div className="div">
                                                <div className="orange-btn-wrapper">
                                                    {typeof(this.selectedCar.price) === 'number' ?
                                                        <a href={"mailto:rent@cartelis.sk?subject=" + this.selectedCar.brand + " " + this.selectedCar.type + ", " + this.props.t("carDetail.fromOnly").toLowerCase() + " "  + this.selectedCar.price + " €/deň"} className="orange-btn">{this.props.t("carDetail.placeOrder")}</a>
                                                        :
                                                        <a href={"mailto:rent@cartelis.sk?subject=" + this.selectedCar.brand + " " + this.selectedCar.type + ", " + this.props.t("carDetail.onRequest")} className="orange-btn">{this.props.t("carDetail.onRequest")}</a>
                                                    }
                                                </div>
                                            </div>
                                        </div>  
                                    </div>
                                </div>

                                <div className="flex flex-center wrap car-detail--prices">
                                    {this.selectedCar.detail.prices.map(price => (
                                        <DarkBox key={price[0] + this.selectedCar.type} numOfDays={this.props.t(price[0])} days={this.props.t(price[1])} price={this.props.t(price[2])} subject={this.selectedCar.brand + " " + this.selectedCar.type + ", " + price[0] + " " + this.props.t(price[1]) + ", " + this.props.t(price[2])}></DarkBox>
                                    ))}
                                </div>
                            </div>
                        </div>
                    </div>
                <Footer></Footer>
            </div>
        );
    }
}

export default withTranslation()(BaseCarDetail);