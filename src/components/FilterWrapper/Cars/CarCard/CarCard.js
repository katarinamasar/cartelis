import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';

import './CarCard.scss';

class BaseCarCard extends Component {
    render() {

        return (
            <>
                <div className="carCard--imageOverlayWrapper">
                    <div className="carCard--imageWrapper">
                        <img className="carCard--image" src={process.env.PUBLIC_URL + this.props.image} alt=""/>
                        <div className="carCard--overlay"></div>   
                    </div>
                </div>
                <div className="carCard--info">
                    <div className="carCard--buttonWrapper">
                        <div className="carCard--button">{this.props.t("offer.moreInfo")}</div>
                    </div>  
                    <div className="flex flex-between">
                        <h2 className="carCard--h2">{this.props.brand}</h2>
                        
                        {typeof(this.props.price) === 'number' ?
                            <h1 className="carCard--h1"><span className="carCard--italic carCard--span">{this.props.t("offer.from")}</span>{this.props.price} € </h1>
                            :
                            <span className="carCard--italic carCard--span carCard--col-center">{this.props.t("carDetail.onRequest")}</span>
                        }
                    </div>
                    <div className="flex flex-between">
                        <p className="carCard--italic">{this.props.type}</p>
                        {typeof(this.props.price) === 'number' ? 
                            <p className="carCard--italic"><span className="carCard--small">{this.props.t("offer.tax")}</span></p>
                            :
                            <></>
                        }
                    </div>
                </div>
            </>
        );
    }
}

export default withTranslation()(BaseCarCard);