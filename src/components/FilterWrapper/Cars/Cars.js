import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { withTranslation } from 'react-i18next';
import i18n from '../../../i18n';

import { animateScroll as scroll } from "react-scroll";

import '../Cars/Cars.scss';

import CarCard from './CarCard/CarCard';
import HeadingMain from '../../ReusableComponents/HeadingMain/HeadingMain';

const data = require('./carList.json');

class BaseCars extends Component {

    state = {
        selectedCar: 1,
        filteredCars: data
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.filteredCars !== this.props.filteredCars){
            this.setState({ filteredCars: nextProps.filteredCars });
          }
    }

    selectCar(id) {
        this.setState({
            selectedCar: id
        });
    }
    
    render() {
        return (
            <section className="cars" name="offer" >
                <HeadingMain sectionName={this.props.t("offer.heading")} type="h1-glow_reverse"></HeadingMain>
                <div className="cars--wrapper">
                    <div className="container">
                        <div className="flex flex-center wrap cars--results">
                            {this.state.filteredCars.length > 0 ? 
                                this.state.filteredCars.map(car => (
                                   <Link 
                                        to={{
                                            pathname: '/' + car.brand + "-" + car.type + "_" + car.id,
                                            search: `lng=${i18n.language}`
                                        }}
                                        key={car.id}
                                        className="carCard"
                                        onClick={() => scroll.scrollToTop({duration: 0})}
                                    >
                                            <CarCard image={car.imageMain} brand={car.brand} price={car.price} type={car.type} pricePer={car.pricePer} key={car.id}/> 
                                    </Link>
                                )) : 
                                <p className="orange">{this.props.t("offer.noResults")}</p>
                            }
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default withTranslation()(BaseCars);