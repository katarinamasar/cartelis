import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';

import HeadingMain from '../../ReusableComponents/HeadingMain/HeadingMain';
import FilterButton from '../Filter/FilterButton/FilterButton';
import './Filter.scss';


class BaseFilter extends Component {
    state = {
        selectedBrand: 'all',
        selectedType: 'all',
        selectedSort: 'priceDesc',
        brandOpen: window.innerWidth < 910 ? false : true,
        typeOpen: window.innerWidth < 910 ? false : true,
        sortOpen: window.innerWidth < 910 ? false : true,
        arrowBrand: 'rotate(0deg)',
        arrowType: 'rotate(0deg)',
        arrowSort: 'rotate(0deg)'
    }

    brand;
    type;
    sort;

    brands = [['all', 'home.all'], ['Toyota', 'home.toyota'], ['Fiat', 'home.fiat'] , ['Mercedes-Benz', 'home.mercedes'], ['Dodge', 'home.dodge'], ['BMW', 'home.bmw'], ['Nissan', 'home.nissan']];
    types = [['all', 'home.all'], ['sedan', 'home.sedan'], ['coupé', 'home.coupe'], ['cabrio', 'home.cabrio'], ['suv', 'home.suv'], ['van', 'home.van'], ['pickup', 'home.pickup']];
    sorts = [['priceDesc', 'home.sortDescPrice'], ['priceAsc', 'home.sortAscPrice'], ['power', 'home.sortPower']];

    arrowStyle;

    onBrandSelected = (brand) => {
        this.brand = brand;
        this.setState({selectedBrand: brand});
    }

    onTypeSelected = (type) => {
        this.type = type;
        this.setState({selectedType: type});
    }
    
    onSortSelected = (sort) => {
        this.sort = sort;
        this.setState({selectedSort: sort});
    }

    onToggle(filterParam) {
        if (window.innerWidth < 910) {
            if (filterParam === 'brand') {
                this.setState( {
                    brandOpen: !this.state.brandOpen,
                    arrowBrand: this.state.arrowBrand === 'rotate(0deg)' ? 'rotate(180deg)' : 'rotate(0deg)'
                } );
            }
            if (filterParam === 'type') {
                this.setState( {
                    typeOpen: !this.state.typeOpen,
                    arrowType: this.state.arrowType === 'rotate(0deg)' ? 'rotate(180deg)' : 'rotate(0deg)'
                } );
            }
            if (filterParam === 'sort') {
                this.setState( {
                    sortOpen: !this.state.sortOpen,
                    arrowSort: this.state.arrowSort === 'rotate(0deg)' ? 'rotate(180deg)' : 'rotate(0deg)'
                } );
            }

        }
    }


    render() {

        return (
            <section className="filter">
                <div name="filter">
                <div className="filter--bgImg"></div>
                    <HeadingMain sectionName={this.props.t("home.heading")} type="headingMain-noLine"></HeadingMain>
                    <div className="container">
                        <div className="filter--filterWrapper flex flex-center wrap">
                            <div className="flex flex-center wrap">
                                <div className="flex-col">
                                    <div className="filter--buttonWrapper-grey"  onClick={() => this.onToggle('brand')}>
                                        <div className="filter--button">{this.props.t("home.brand")}</div>
                                        <div className="arrow-down">
                                            <img src={process.env.PUBLIC_URL + '/images/arrow.png'} alt=""  style={{ transform: this.state.arrowBrand}}/>
                                        </div>
                                    </div>
                                    <div className={[this.state.brandOpen ? "open" : "", "closed"].join(" ")}>
                                        {this.brands.map( brand =>  (
                                            <FilterButton type={brand[0]} text={brand[1]} clicked={this.onBrandSelected} key={brand[0]} active={brand[0] === this.state.selectedBrand}></FilterButton>
                                            ))}
                                    </div>
                                </div>
                                <div className="flex-col">
                                    <div className="filter--buttonWrapper-grey" onClick={() => this.onToggle('type')}>
                                        <div className="filter--button">{this.props.t("home.type")}</div>
                                        <div className="arrow-down">
                                            <img src={process.env.PUBLIC_URL + '/images/arrow.png'} alt="" style={{ transform: this.state.arrowType}}/>
                                        </div>
                                    </div>
                                    <div className={[this.state.typeOpen ? "open" : "", "closed"].join(" ")}>
                                        {this.types.map( type =>  (
                                            <FilterButton type={type[0]} text={type[1]}clicked={this.onTypeSelected} key={type[0]} active={type[0] === this.state.selectedType}></FilterButton>
                                            ))}
                                    </div>
                                </div>
                                <div className="flex-col">
                                    <div className="filter--buttonWrapper-grey" onClick={() => this.onToggle('sort')}>
                                        <div className="filter--button">{this.props.t("home.sort")}</div>
                                        <div className="arrow-down">
                                            <img src={process.env.PUBLIC_URL + '/images/arrow.png'} alt="" style={{ transform: this.state.arrowSort}}/>
                                        </div>
                                    </div>
                                    <div className={[this.state.sortOpen ? "openSmall" : "", "closed"].join(" ")}>
                                        {this.sorts.map( sort =>  (
                                            <FilterButton type={sort[0]} text={sort[1]} clicked={this.onSortSelected} key={sort[0]} active={sort[0] === this.state.selectedSort}></FilterButton>
                                            ))}
                                    </div>
                                </div>
                                <div className="flex-col">
                                    <div className="filter--buttonWrapper-orange" onClick={() => this.props.filterConditions(this.brand, this.type, this.sort)}>
                                        <div className="filter--button">{this.props.t("home.search")}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default withTranslation()(BaseFilter);