import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';

class BaseFilterButton extends Component {

    render() {
        return (
            <div className={['filter--buttonWrapper', this.props.active ? 'filterBtn--active' : ''].join(" ") } onClick={() => this.props.clicked(this.props.type)}>
                <div className="filter--button">{this.props.t(this.props.text)}</div>
            </div>
        );
    }
}

export default withTranslation()(BaseFilterButton);