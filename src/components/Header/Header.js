import React, { Component } from 'react';
import Select from 'react-select';

import { animateScroll as scroll } from "react-scroll";
import NavigLink from './NavigLink/NavigLink';

import './Header.scss';

import { withTranslation } from 'react-i18next';
import i18n from '../../i18n';
import { withRouter } from 'react-router-dom';


const changeLanguage = (lang) => {
    i18n.changeLanguage(lang);
}

const options = [
    { value: 'sk', label: "SK" },
    { value: 'en', label: "EN" },
    { value: 'de', label: "DE" },
    { value: 'fr', label: "FR" }
];

const customStyles = {
    container: (provided, state) => ({
        ...provided,
        width: '75px',
        border: state.selectProps.menuIsOpen ? '1px solid #c43c00' : '1px solid transparent',
        borderBottom: '0'
    }),
    control:(provided, state) => ({
        ...provided,
        backgroundColor: 'black',
        border: '0',
        boxShadow: '0 transparent',
        cursor: 'pointer',
        '&:hover': { 
            border: '0',
            boxShadow: '0 transparent'
        }
    }),
    option: (provided, state) => ({
        ...provided,
        padding: '3px 13px',
        cursor: 'pointer',
        textAlign: 'left',
        color: 'white',
        fontWeight: '700',
        display: state.isSelected ? 'none' : 'inline-block',
        backgroundColor: 'black',
        '&:hover': { 
            color: '#c43c00',
            backgroundColor: 'transparent'     
        }
    }),
    indicatorSeparator: (provided, state) => ({
        ...provided,
        backgroundColor: 'transparent'
    }),
    dropdownIndicator: (provided, state) => ({
        ...provided,
        paddingBottom: '11px',
        paddingLeft: '3px'
    }),
    menu: (provided, state) => ({
        ...provided,
        width: '75px',
        left: '-1px',
        backgroundColor: 'black',
        borderRadius: 0,
        border: '1px solid #c43c00',
        borderTop: 0,
        marginTop: 0
    }),
    singleValue: (provided, state) => {
        const imgSrc = '/images/flag_' + state.data.value + '.jpg';
        const background = 'url("' + imgSrc + '") no-repeat center center';
        const backgroundSize = 'contain';
        const color = "transparent";
        const width = "70%";

        return { ...provided, background, backgroundSize, color, width };
    }
};


class BaseHeader extends Component {

    constructor(props) {
        super(props);
        i18n.on('languageChanged', (lng) => {
            this.setState({ selectedOption: lng });
        });
    }
    
    state = {
        mobileMenuOpen: window.innerWidth < 910 ? false : true,
        selectedOption: i18n.language
    };

    handleChange = selectedOption => {
        changeLanguage(selectedOption.value);
        this.props.history.push({
            search: `lng=${selectedOption.value}`
        });
        this.setState(
            { selectedOption: selectedOption.value }
        );
    };


    toggleMenu = () => {
        if(window.innerWidth < 910) {                                               // pri 910px zacina mobilne menu
            this.setState({ mobileMenuOpen: !this.state.mobileMenuOpen });
        }
    }
      
    
    render() {
        return (
            <div className="header">
                <img src={process.env.PUBLIC_URL + '/images/logo_header-mobile.png'} alt="logo" className="header--logo-mobile"/>

                <div className="container">

                    <div className="header--desktop">
                        <div className="italic header--openHours">
                            <div className="flex flex-between flex-align-center">
                                <div className="flex socials">
                                    <a href="https://www.facebook.com/pages/category/Car-Rental/cartelisrent-108117387403049/" className="fb">fb</a>
                                    <a href="https://www.instagram.com/cartelis.rent/?hl=sk" className="insta">ig</a>
                                </div>

                                <div className="flex flex-align-center">
                                    <div>{this.props.t("header.openHours")} 9:00 - 20:00</div>
                                    <div className="lang-dropdown">
                                        <Select
                                            value={ options.find(option => option.value === this.state.selectedOption) }
                                            onChange={this.handleChange}
                                            options={options}
                                            styles={customStyles}
                                        />
                                    </div>
                                </div>
                            </div> 
                        </div>
                        
                        <div className="header--logoFull">
                            <img src={process.env.PUBLIC_URL + '/images/logo_header.png'} alt="logo" className="header--logo"/>
                            <img src={process.env.PUBLIC_URL + '/images/logo_header-small.png'} alt="logo" className="header--logo-small"/>
                        </div>

                        <nav className={["flex", "flex-around"].join(" ")}>
                            <ul className="flex header--ul-left">
                                <li>
                                    <NavigLink sectionName={this.props.t("header.menu.home")} to="filter" offset={-240} duration={500} onClick={() => this.toggleMenu()}></NavigLink>
                                </li>
                                <li>
                                    <NavigLink sectionName={this.props.t("header.menu.offer")} to="offer" offset={-150} duration={500} onClick={() => this.toggleMenu()}></NavigLink> 
                                </li>
                                <li>                             
                                    <NavigLink sectionName={this.props.t("header.menu.services")} to="services" offset={-150} duration={500} onClick={() => this.toggleMenu()}></NavigLink>
                                </li>
                            </ul>

                            <div className="header--divider"></div>
                            
                            <div className="header--to-top" onClick={() => scroll.scrollToTop()}></div>

                            <ul className="flex header--ul-right">
                                <li>
                                    <NavigLink sectionName={this.props.t("header.menu.conditions")} to="conditions" offset={-150} duration={500} onClick={() => this.toggleMenu()}></NavigLink>
                                </li>
                                <li>                             
                                    <NavigLink sectionName={this.props.t("header.menu.aboutUs")} to="aboutUs" offset={-150} duration={500} onClick={() => this.toggleMenu()}></NavigLink>
                                </li>
                                <li>
                                    <NavigLink sectionName={this.props.t("header.menu.contact")} to="contact" offset={-150} duration={700} onClick={() => this.toggleMenu()}></NavigLink>
                                </li>
                            </ul>

                        </nav>

                    </div>

                    <div className="header--mobile">
                        <div className="header--logoFull">
                            <img src={process.env.PUBLIC_URL + '/images/logo_header-small.png'} alt="logo" className="header--logo-small"/>
                        </div>

                        <nav className={["flex", "flex-around", (this.state.mobileMenuOpen ? "" : "hide")].join(" ")}>
                            <ul className="flex header--ul-left">
                                <li>
                                    <NavigLink sectionName={this.props.t("header.menu.home")} to="filter" offset={-240} duration={500} onClick={() => this.toggleMenu()}></NavigLink>
                                </li>
                                <li>
                                    <NavigLink sectionName={this.props.t("header.menu.offer")} to="offer" offset={-150} duration={500} onClick={() => this.toggleMenu()}></NavigLink> 
                                </li>
                                <li>                             
                                    <NavigLink sectionName={this.props.t("header.menu.services")} to="services" offset={-150} duration={500} onClick={() => this.toggleMenu()}></NavigLink>
                                </li>
                            </ul>

                            <div className="header--divider"></div>
                            
                            <div className="header--to-top" onClick={() => scroll.scrollToTop()}></div>

                            <ul className="flex header--ul-right">
                                <li>
                                    <NavigLink sectionName={this.props.t("header.menu.conditions")} to="conditions" offset={-150} duration={500} onClick={() => this.toggleMenu()}></NavigLink>
                                </li>
                                <li>                             
                                    <NavigLink sectionName={this.props.t("header.menu.aboutUs")} to="aboutUs" offset={-150} duration={500} onClick={() => this.toggleMenu()}></NavigLink>
                                </li>
                                <li>
                                    <NavigLink sectionName={this.props.t("header.menu.contact")} to="contact" offset={-150} duration={700} onClick={() => this.toggleMenu()}></NavigLink>
                                </li>
                            </ul>
                        </nav>
                        <div className="mobile-lang">
                            <Select
                                value={ options.find(option => option.value === this.state.selectedOption) }
                                onChange={this.handleChange}
                                options={options}
                                styles={customStyles}
                            />
                        </div>
                    </div>

                    
                    <div className="flex flex-end">
                        
                        <div className={["mobileMenu", (this.state.mobileMenuOpen ? "change" : "")].join(' ')} onClick={() => this.toggleMenu()}>
                            <div className="bar1"></div>
                            <div className="bar3"></div>
                        </div>
                    </div>
                </div>
                
            </div>
        );
    }
}


export default withRouter(withTranslation()(BaseHeader));
 