import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Select from 'react-select';

import { animateScroll as scroll, scroller } from "react-scroll";
import { withTranslation } from 'react-i18next';
import { withRouter } from 'react-router-dom';
import i18n from '../../i18n';

import './Header.scss';

const changeLanguage = (lang) => {
    i18n.changeLanguage(lang);
}

const options = [
    { value: 'sk', label: "SK" },
    { value: 'en', label: "EN" },
    { value: 'de', label: "DE" },
    { value: 'fr', label: "FR" }
];

const customStyles = {
    container: (provided, state) => ({
        ...provided,
        width: '75px',
        border: state.selectProps.menuIsOpen ? '1px solid #c43c00' : '1px solid transparent',
        borderBottom: '0'
    }),
    control:(provided, state) => ({
        ...provided,
        backgroundColor: 'black',
        border: '0',
        boxShadow: '0 transparent',
        cursor: 'pointer',
        '&:hover': { 
            border: '0',
            boxShadow: '0 transparent'
        }
    }),
    option: (provided, state) => ({
        ...provided,
        padding: '3px 13px',
        cursor: 'pointer',
        textAlign: 'left',
        color: 'white',
        fontWeight: '700',
        display: state.isSelected ? 'none' : 'inline-block',
        backgroundColor: 'black',
        '&:hover': { 
            color: '#c43c00',
            backgroundColor: 'transparent'     
        }
    }),
    indicatorSeparator: (provided, state) => ({
        ...provided,
        backgroundColor: 'transparent'
    }),
    dropdownIndicator: (provided, state) => ({
        ...provided,
        paddingBottom: '11px',
        paddingLeft: '3px'
    }),
    menu: (provided, state) => ({
        ...provided,
        width: '75px',
        left: '-1px',
        backgroundColor: 'black',
        borderRadius: 0,
        border: '1px solid #c43c00',
        borderTop: 0,
        marginTop: 0
    }),
    singleValue: (provided, state) => {
        const imgSrc = '/images/flag_' + state.data.value + '.jpg';
        const background = 'url("' + imgSrc + '") no-repeat center center';
        const backgroundSize = 'contain';
        const color = "transparent";
        const width = "70%";

        return { ...provided, background, backgroundSize, color, width };
    }
};

class BaseHeader2 extends Component {

    constructor(props) {
        super(props);
        i18n.on('languageChanged', (lng) => {
            this.setState({ selectedOption: lng });
        });
    }

    state = {
        mobileMenuOpen: window.innerWidth < 910 ? false : true,
        selectedOption: i18n.language
    };

    handleChange = selectedOption => {
        changeLanguage(selectedOption.value);
        this.props.history.push({
            search: `lng=${selectedOption.value}`
        });
        this.setState(
            { selectedOption: selectedOption.value }
        );
    };

    toggleMenu = (sectionName, offset) => {
        if(window.innerWidth < 910) {                                               // pri 910px zacina mobilne menu
            this.setState({ mobileMenuOpen: !this.state.mobileMenuOpen });
        }

        scroller.scrollTo(sectionName, {duration: 0, delay: 0, offset: offset});
    }

    render() {
        let aboutUsText = this.props.t("header.menu.aboutUs");

        return (
            <div className="header">
                <img src={process.env.PUBLIC_URL + '/images/logo_header-mobile.png'} alt="logo" className="header--logo-mobile"/>

                <div className="container">

                    <div className="header--desktop">
                        <div className="italic header--openHours">
                            <div className="flex flex-between flex-align-center">
                                <div className="flex socials">
                                    <a href="https://www.facebook.com/pages/category/Car-Rental/cartelisrent-108117387403049/" className="fb">fb</a>
                                    <a href="https://www.instagram.com/cartelis.rent/?hl=sk" className="insta">ig</a>
                                </div>

                                <div className="flex flex-align-center">
                                    <div>{this.props.t("header.openHours")} 9:00 - 20:00</div>
                                    <div className="lang-dropdown">
                                        <Select
                                            value={ options.find(option => option.value === this.state.selectedOption) }
                                            onChange={this.handleChange}
                                            options={options}
                                            styles={customStyles}
                                        />
                                    </div>
                                </div>
                            </div> 
                        </div>
                        
                        <div className="header--logoFull">
                            <img src={process.env.PUBLIC_URL + '/images/logo_header.png'} alt="logo" className="header--logo"/>
                            <img src={process.env.PUBLIC_URL + '/images/logo_header-small.png'} alt="logo" className="header--logo-small"/>
                        </div>

                        <nav className={["flex", "flex-around"].join(" ")}>
                            <ul className="flex header--ul-left">
                                <li>
                                    <Link className="header--nav-link" to="/" onClick={() => this.toggleMenu('filter', -240)}><span className="header--link">{this.props.t("header.menu.home")}</span></Link>
                                </li>
                                <li>
                                    <Link className="header--nav-link header--active" to="/" onClick={() => this.toggleMenu('offer', -150)}><span className="header--link">{this.props.t("header.menu.offer")}</span></Link> 
                                </li>
                                <li>                             
                                    <Link className="header--nav-link" to="/" onClick={() => this.toggleMenu('services', -150)}><span className="header--link">{this.props.t("header.menu.services")}</span></Link>
                                </li>
                            </ul>

                            <div className="header--divider"></div>
                            
                            <div className="header--to-top" onClick={() => scroll.scrollToTop()}></div>

                            <ul className="flex header--ul-right">
                                <li>
                                    <Link className="header--nav-link" to="/" onClick={() => this.toggleMenu('conditions', -150)}><span className="header--link">{this.props.t("header.menu.conditions")}</span></Link>
                                </li>
                                <li>                             
                                    <Link className="header--nav-link" to="/" onClick={() => this.toggleMenu('aboutUs', -150)}><span className="header--link" dangerouslySetInnerHTML={ { __html: aboutUsText } }></span></Link>
                                </li>
                                <li>
                                    <Link className="header--nav-link" to="/" onClick={() => this.toggleMenu('contact', -150)}><span className="header--link">{this.props.t("header.menu.contact")}</span></Link>
                                </li>
                            </ul>

                        </nav>

                    </div>

                    <div className="header--mobile">
                        <div className="header--logoFull">
                            <img src={process.env.PUBLIC_URL + '/images/logo_header-small.png'} alt="logo" className="header--logo-small"/>
                        </div>
                        <nav className={["flex", "flex-around", (this.state.mobileMenuOpen ? "" : "hide")].join(" ")}>
                            <ul className="flex header--ul-left">
                                <li>
                                    <Link className="header--nav-link" to="/" onClick={() => this.toggleMenu('filter', -240)}><span className="header--link">{this.props.t("header.menu.home")}</span></Link>
                                </li>
                                <li>
                                    <Link className="header--nav-link header--active" to="/" onClick={() => this.toggleMenu('offer', -150)}><span className="header--link">{this.props.t("header.menu.offer")}</span></Link> 
                                </li>
                                <li>                             
                                    <Link className="header--nav-link" to="/" onClick={() => this.toggleMenu('services', -150)}><span className="header--link">{this.props.t("header.menu.services")}</span></Link>
                                </li>
                            </ul>

                            <div className="header--divider"></div>
                            
                            <div className="header--to-top" onClick={() => scroll.scrollToTop()}></div>

                            <ul className="flex header--ul-right">
                                <li>
                                    <Link className="header--nav-link" to="/" onClick={() => this.toggleMenu('conditions', -150)}><span className="header--link">{this.props.t("header.menu.conditions")}</span></Link>
                                </li>
                                <li>                             
                                    <Link className="header--nav-link" to="/" onClick={() => this.toggleMenu('aboutUs', -150)}><span className="header--link" dangerouslySetInnerHTML={ { __html: aboutUsText } }></span></Link>
                                </li>
                                <li>
                                    <Link className="header--nav-link" to="/" onClick={() => this.toggleMenu('contact', -150)}><span className="header--link">{this.props.t("header.menu.contact")}</span></Link>
                                </li>
                            </ul>
                        </nav>
                        <div className="mobile-lang">
                            <Select
                                value={ options.find(option => option.value === this.state.selectedOption) }
                                onChange={this.handleChange}
                                options={options}
                                styles={customStyles}
                            />
                        </div>
                    </div>
                    
                    <div className="flex flex-end">
                        <div className={["mobileMenu", (this.state.mobileMenuOpen ? "change" : "")].join(' ')} onClick={() => this.toggleMenu()}>
                            <div className="bar1"></div>
                            <div className="bar3"></div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default withRouter(withTranslation()(BaseHeader2));