import React, { Component } from 'react';

import { Link } from "react-scroll";

import './NavigLink.scss';

class NavigLink extends Component {
    render() {
        let sectionNameProp = this.props.sectionName;

        return (
            <Link className="header--nav-link" activeClass="header--active" to={this.props.to} spy smooth offset={this.props.offset} duration={this.props.duration} onClick={this.props.onClick}>
                <span className="header--link" dangerouslySetInnerHTML={ { __html: sectionNameProp } }></span>
            </Link>
        );
    }
}

export default NavigLink;
