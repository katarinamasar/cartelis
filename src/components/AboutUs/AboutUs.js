import React, { Component } from 'react';

import HeadingMain from '../ReusableComponents/HeadingMain/HeadingMain';
import Service from '../Services/Service/Service';

import { withTranslation } from 'react-i18next';

import '../Conditions/Conditions.scss';


const data = require('./aboutus.json');



class BaseAboutUs extends Component {
    render() {
        return (
            <section name="aboutUs" className="section-pb">
                <HeadingMain sectionName={this.props.t("aboutUs.heading")} type="h1-scewed-bordered"></HeadingMain>
                <div className="container">
                    {data.map(c => (
                        <div className="services--service">
                            <Service heading={this.props.t(c.heading)} text={this.props.t(c.text)} key={c.heading} type="condition about"  /> 
                        </div> ))}
                </div>
            </section>
        );
    }
}

export default withTranslation()(BaseAboutUs);