import React, { Component } from 'react';

import HeadingMain from '../ReusableComponents/HeadingMain/HeadingMain'; 
import { withTranslation } from 'react-i18next';

import './Contact.scss';


class BaseContact extends Component {
    render() {
        return (
            <section name="contact">
                <HeadingMain sectionName={this.props.t("header.menu.contact")} type="h1-scewed-bordered"></HeadingMain>
                
                <div className="container contact--info">
                    <div className="flex flex-between wrap">
                        <div className="contact--column">
                            <h3 className="contact--heading">{this.props.t("contact.address")}</h3>
                            <div>
                                <p>CARTELIS s. r. o.</p>
                                <p>Hraničná 16</p> 
                                <p>821 05 Bratislava</p>
                                <p>{this.props.t("contact.slovakia")}</p>
                            </div>
                        </div>

                        <div className="contact--column">
                            <h3 className="contact--heading">{this.props.t("contact.openHours")}</h3>
                            <div>
                                <p>{this.props.t("contact.openDays")}</p>
                                <p>9:00 - 20:00</p> 
                                <br></br>
                                <p>{this.props.t("contact.openSpecial")}</p>
                            </div>
                        </div>

                        <div className="contact--column contact--contact">
                            <h3 className="contact--heading">{this.props.t("header.menu.contact")}</h3>
                            <div>
                                <p>+421 902 649 603</p>
                                <p>+421 945 451 831</p> 
                                <p>e: <a href="mailto:rent@cartelis.sk">rent@cartelis.sk</a></p>
                            </div>
                        </div>

                        <div className="contact--column">
                            <h3 className="contact--heading">{this.props.t("contact.invoiceDetails")}</h3>
                            <div>
                                <p>CARTELIS s. r. o.</p>
                                <p>Nezábudková 5</p> 
                                <p>821 01 Bratislava - mestská časť Ružinov</p>
                                <p>{this.props.t("contact.ico")}: 52676382</p>
                                <p>{this.props.t("contact.dic")}: 2121150427</p> 
                                <p>IBAN: SK24 1100 0000 0029 4008 2973</p>
                            </div>
                        </div>
                    </div>

                    <div className="contact--socials flex flex-center">
                        <a href="https://www.facebook.com/pages/category/Car-Rental/cartelisrent-108117387403049/" className="contact--fb">fb</a>
                        <a href="https://www.instagram.com/cartelis.rent/?hl=sk" className="contact--insta">ig</a>
                    </div>

                    <div className="contact--map-wrapper">
                        <a href="https://www.google.com/maps/place/Hrani%C4%8Dn%C3%A1+16,+821+05+Bratislava/@48.1457819,17.1535941,17z/data=!3m1!4b1!4m5!3m4!1s0x476c88d7a69ae9f3:0x533e42d59ea8184c!8m2!3d48.1457819!4d17.1557828" className="contact--map" target="_bank">
                            {/* <img src={process.env.PUBLIC_URL + '/images/map.png'} alt="" className="contact--map"/> */}
                        </a>
                    </div>
                </div>
            </section>
        );
    }
}

export default withTranslation()(BaseContact);