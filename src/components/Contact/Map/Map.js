import React, { Component } from 'react';
import { withGoogleMap, GoogleMap, withScriptjs, Marker } from 'react-google-maps';

import './Map.scss';
import mapStyles from './mapStyles';


class Map extends Component {
   render() {
        const WrappdMap = withScriptjs(withGoogleMap(props => (
            <GoogleMap
                defaultCenter = { { lat: 48.15103, lng: 17.15157 } }
                defaultZoom = { 13 }
                defaultOptions={{ styles: mapStyles }}
            >
                <Marker 
                    position={ { lat: 48.15103, lng: 17.15157 } } 
                    icon={process.env.PUBLIC_URL + '/images/map_marker_upr.png'}
                    />
            </GoogleMap>
        )));

        return(
            <div className="map--wrapper">
                <WrappdMap
                googleMapURL={`https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places`}
                loadingElement={ <div style={{ height: `100%`}} /> }
                containerElement={ <div style={{ height: `100%`}} /> }
                mapElement={ <div style={{ height: `100%` }} /> }
                />
            </div>
        );
   }
};

export default Map;