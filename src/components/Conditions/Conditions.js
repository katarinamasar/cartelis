import React, { Component } from 'react';

import HeadingMain from '../ReusableComponents/HeadingMain/HeadingMain';
import Service from '../Services/Service/Service';
import { Element } from 'react-scroll';
import { withTranslation } from 'react-i18next';

import './Conditions.scss';


const data = require('./conditions.json');



class BaseConditions extends Component {
    state = {
        open: false, 
        returnPolicyOpen: false
    };

    open;
    btnText;

    returnPolicy = Object.values(data).find(cond => cond.heading === "Reklamačný poriadok");

    toggleShowHide() {
        this.setState({open: !this.state.open});
    }


    render() {
        this.open = this.state.open ? "conditions-shown" : "conditions-hidden";
        this.btnText = this.state.open ? this.props.t("conditions.hideTerms") : this.props.t("conditions.showAllTerms");

        return (
            <Element name="conditions" className="section-pb conditions">
                <div className={[this.open].join(" ")}>
                    <HeadingMain sectionName={this.props.t("conditions.heading")} type="h1-scewed-bordered"></HeadingMain>
                    <div className="container">
                        <div>
                            {Object.values(data).map(c => (
                                <div className="services--service" key={c.heading}>
                                    <Service heading={this.props.t(c.heading)} text={this.props.t(c.text)} type="condition" /> 
                                </div> ))}
                        </div>

                        <div className="conditions--show-hide">
                            <div className="center-wrapper">
                                <div className="conditions--buttonWrapper" onClick={() => this.toggleShowHide()}>
                                    <div className="conditions--button">{this.btnText}</div>
                                </div>        
                            </div>
                        </div>
                    </div>
                </div>
            </Element>
        );
    }
}

export default withTranslation()(BaseConditions);