import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';

import './Service.scss';


class BaseService extends Component {
    render() {

        let text = this.props.text;

        return (
            <div className={["service--wrapper", this.props.type].join(" ")}> 
                <div className="service--content">
                    <h2>{this.props.heading}</h2>
                    {this.props.text ? 
                        <p dangerouslySetInnerHTML={ { __html: text } }></p> : 
                        <a href={process.env.PUBLIC_URL + this.props.t("conditions.pathToClaim")} className="orange-hover">{this.props.t("conditions.downloadClaim")}</a>
                    }
                </div>
            </div>
        );
    }
}

export default withTranslation()(BaseService);