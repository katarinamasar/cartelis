import React, { Component } from 'react';

import HeadingMain from '../ReusableComponents/HeadingMain/HeadingMain';
import Service from './Service/Service';
import { withTranslation } from 'react-i18next';

import './Services.scss';

const data = require('./services.json');


class BaseServices extends Component {
    render() {
        return (
            <section name="services" className="section-pb">
                <HeadingMain sectionName={this.props.t("services.heading")} type="h1-scewed-bordered"></HeadingMain>
                <div className="container">
                    <div className="services--wrapper">
                        <div className="services--bg-grey"></div>
                        <div>
                            {Object.values(data).map(s => (
                                <div className="services--service" key={s.heading}>
                                    <Service heading={this.props.t(s.heading)} text={this.props.t(s.text)} type="service" /> 
                                </div> ))}
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default withTranslation()(BaseServices);